package parser

import (
	"net/http"
	"github.com/PuerkitoBio/goquery"
	"os"
	"bufio"
	"log"
	"time"
	"math/rand"
	"net/url"
	"sync"
	"regexp"
	"strings"
	"github.com/imdario/mergo"
	"io/ioutil"
	"golang.org/x/oauth2/google"
	"context"
	"google.golang.org/api/sheets/v4"
	"fmt"
)

type Company struct{
	mutex sync.Mutex
	Name string
	Phones []string
	Emails []string
	Website string
}
var proxyList []string;
func getProxyList(){
	if file, err := os.Open("/home/vova/Projects/2GISParser/proxy.txt"); err==nil{
		defer file.Close()

		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			proxyList = append(proxyList,scanner.Text())
		}
		if err = scanner.Err(); err != nil {
			log.Fatal(err)
		}
	} else {
		log.Fatal(err)
	}
}
func NewDocument(link string) (*goquery.Document,error){
	timeout:= time.Duration(5*time.Second)
	rand.Seed(time.Now().UTC().UnixNano())
	proxy:= proxyList[rand.Intn(len(proxyList))]
	proxyUrl,err:=url.Parse("http://"+proxy)
	if err!=nil{
		log.Fatal(err)
	}
	client := &http.Client{Transport: &http.Transport{Proxy: http.ProxyURL(proxyUrl)},Timeout:timeout}
	res, e := client.Get(link)

	if (e != nil)||(res.StatusCode!=200) {
		return NewDocument(link)
	}
	return goquery.NewDocumentFromResponse(res)
}
const TARGET_URL string = "https://2gis.ua"

var possibleContactPagesTitles []string = []string{"контакти","контакты","о компании","о нас","контактная информация","контактна інформація"}

func QueryParse(query string){
	getProxyList()
	link := TARGET_URL+"/kiev/search/"+query + "/"
	document,err:=NewDocument(link)
	if(err!=nil){
		QueryParse(query)
	}
	for err == nil {
		var wg sync.WaitGroup
		var page_companies chan Company = make(chan Company,25)
		document.Find(".miniCard__headerTitleLink").Each(func(i int, s *goquery.Selection) {
			company_url, exists := s.Attr("href")
			if (exists) {
				wg.Add(1)
				go parseCompany(company_url,&wg,page_companies)
			}
		})
		wg.Wait()
		close(page_companies)
		var companies []Company
		for company := range page_companies {
			companies = append(companies,company)
		}
		writeCompaniesInfoToSpreadSheet("18UI-swVxXQ18YZNLncjwiAyHOv_a2mw_4MV4Jig1MtI","Строительные компании",companies)

		next_page_link, _ := document.Find(".pagination__page._current + .pagination__page").Attr("href")
		document, err = NewDocument(TARGET_URL+next_page_link)
		if(err!=nil){
			document, err = NewDocument(TARGET_URL+next_page_link)
			if(err!=nil){
				log.Println(link)
				log.Println("ERROR")
			}
		}
	}

}
func parseCompany(company_url string,wg *sync.WaitGroup,page_companies chan Company) {
	company_document, err_company := NewDocument(TARGET_URL+company_url)
	if (err_company != nil) {
		parseCompany(company_url,wg,page_companies)

	}

	company := Company{}
	if(company_document!=nil) {
		defer wg.Done()

		company.Website = company_document.Find(".contact__websites a").First().Text()
		if len(company.Website) != 0 {
			parseCompanyWebsite("http://" + company.Website, &company)

		}
		company.Name = company_document.Find(".cardHeader__headerNameText").Text()
		company_document.Find(".contact__phonesVisible .contact__phonesItemLinkNumber").Each(func(i int, s *goquery.Selection) {
			phone := s.Text()[1:len(s.Text())]
			company.Phones = append(company.Phones, phone)
		})
	}
	page_companies <- company
}
func parseCompanyWebsite(company_website string,company *Company){
	document,err:= goquery.NewDocument(company_website)
	if(err!=nil){
		document,err = NewDocument(company_website)
		if(err!=nil){
			return
		}
	}
	emailRegex,err:= regexp.Compile(`(?:[a-z0-9!#$%&'*+/=?^_{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])`)
	if(err!=nil){
		log.Fatal(err)
	}
	website_company :=Company{}
	var wg sync.WaitGroup
	wg.Add(1)
	go func(document *goquery.Document,emailRegex *regexp.Regexp,company_website string,website_company *Company) {
		defer wg.Done()
		document.Find("a").Each(func(i int, s *goquery.Selection) {
			if(stringInSlice(strings.ToLower(s.Text()),possibleContactPagesTitles)){
				link,exists := s.Attr("href");
				if(exists){

					page_document, err_doc := goquery.NewDocument(company_website+link)
					if(err_doc !=nil){
						page_document, err_doc = goquery.NewDocument(company_website+"/"+link)
						if(err_doc !=nil){
							page_document, err_doc = goquery.NewDocument(link)

						}
					}
					document_html, err := page_document.Html();
					if err !=nil {
						log.Fatal(err_doc)
					}
					website_company.Emails = append(website_company.Emails,emailRegex.FindAllString(document_html,-1)...)
				}
			}
		})
	}(document,emailRegex,company_website,&website_company)
	document_html,err:= document.Html();
	if err!=nil {
		log.Fatal(err)
	}

	wg.Wait()

	website_company.Emails = append(website_company.Emails,emailRegex.FindAllString(document_html,-1)...)
	if err:=mergo.Merge(company,website_company); err!=nil{
		log.Fatal(err)
	}
	company.Emails = removeDuplicates(company.Emails)


}
func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}
func removeDuplicates(elements []string) []string {
	encountered := map[string]bool{}

	for v:= range elements {
		encountered[elements[v]] = true
	}
	result := []string{}
	for key, _ := range encountered {
		result = append(result, key)
	}
	return result
}
func writeCompaniesInfoToSpreadSheet(spreadsheetId string, sheetTitle string,companies []Company){
	current_companies:= getCurrentCompanies(spreadsheetId,sheetTitle)
	for _,company:=range companies{
		if(!stringInSlice(company.Name,current_companies)){
			appendCompanyToTable(spreadsheetId,sheetTitle,company)
		}
	}
}
func appendCompanyToTable(spreadsheetId string, sheetTitle string,company Company){
	input_range := sheetTitle+"!A2:C"
	phones_count:=len(company.Phones)
	emails_count:=len(company.Emails)
	var values [][]interface{}
	if(phones_count>=emails_count){
		for i:=0; i<emails_count;i++{
			row:=[]interface{}{`=HYPERLINK("`+company.Website +`";"`+company.Name+`")`,company.Phones[i],company.Emails[i]}
			values=append(values,row)
		}
		for i:=emails_count; i<phones_count;i++{
			row:=[]interface{}{`=HYPERLINK("`+company.Website +`";"`+company.Name+`")`,company.Phones[i],""}
			values=append(values,row)
		}
	}else {
		for i:=0; i<phones_count;i++{
			row:=[]interface{}{`=HYPERLINK("`+company.Website +`";"`+company.Name+`")`,company.Phones[i],company.Emails[i]}
			values=append(values,row)
		}
		for i:=phones_count; i<emails_count;i++{
			row:=[]interface{}{`=HYPERLINK("`+company.Website +`";"`+company.Name+`")`,"",company.Emails[i]}
			values=append(values,row)
		}
	}
	fmt.Println(values)
	srv:=getSheetsService()
	value_range:= sheets.ValueRange{Range:input_range,MajorDimension:"ROWS",Values:values}
	_,err:= srv.Spreadsheets.Values.Append(spreadsheetId,input_range,&value_range).ValueInputOption("USER_ENTERED").Do()

	if err!=nil {
		log.Println(err)
	}

}
func getCurrentCompanies(spreadsheetId string, sheetTitle string) []string{
	var current_companies []string
	srv:= getSheetsService()
	readRange:=sheetTitle+"!A2:A"
	srv.Spreadsheets.Values.Get(spreadsheetId,readRange).Do()
	resp, err := srv.Spreadsheets.Values.Get(spreadsheetId, readRange).Do()
	if err != nil {
		log.Fatalf("Unable to retrieve data from sheet. %v", err)
	}
	if len(resp.Values) > 0 {
		for _, row := range resp.Values {
			current_companies = append(current_companies,(row[0]).(string))
		}
	}
	return current_companies
}
func getSheetsService() *sheets.Service{
	ctx := context.Background()

	b, err := ioutil.ReadFile("/home/vova/Projects/2GISParser/client_secret.json")
	if err != nil {
		log.Fatalf("Unable to read client secret file: %v", err)
	}

	// If modifying these scopes, delete your previously saved credentials
	// at ~/.credentials/sheets.googleapis.com-go-quickstart.json
	config, err := google.ConfigFromJSON(b, "https://www.googleapis.com/auth/spreadsheets.readonly",
		sheets.SpreadsheetsScope,
	)
	if err != nil {
		log.Fatalf("Unable to parse client secret file to config: %v", err)
	}
	client := getClient(ctx, config)

	srv, err := sheets.New(client)
	if err != nil {
		log.Fatalf("Unable to retrieve Sheets Client %v", err)
	}
	return srv
}